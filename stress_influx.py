import json, sys, getopt, time, random, datetime, sched, codecs, signal
from time import sleep
from multiprocessing import Process
from threading import Thread
import requests

def generateTimeStamp(timestamp):
	return "{}".format(timestamp * 1000000000)

class Logger:
	log_file = "message.log"
	ERROR = "[m[1;31mError[m"
	INFO = "[m[1;32mInfo[m"
	DEBUG = "[m[1;33mDebug[m"

	@staticmethod
	def log(level, msg):
		with codecs.open(Logger.log_file, 'a', encoding='utf8') as file:
			output = u"{} [{}] {}".format(str(datetime.datetime.now()), level, msg)
			print(output)
			if not output.endswith("\n"):
				output += "\n"
			file.write(output)

	@staticmethod
	def info(msg):
		Logger.log(Logger.INFO, msg)

	@staticmethod
	def error(msg):
		Logger.log(Logger.ERROR, msg)

	@staticmethod
	def debug(msg):
		Logger.log(Logger.DEBUG, msg)


class Timer:
	def __init__(self):
		self.timerCount = 0
		self.timerStart = 0
		self.pauseCount = 0
		self.pauseStart = 0

	def start(self):
		self.timerCount = 0
		self.pauseCount = 0
		self.timerStart = time.time()

	def end(self):
		self.timerCount = (time.time() - self.timerStart)

	def pause(self):
		self.pauseStart = time.time()

	def resume(self):
		self.pauseCount += (time.time() - self.pauseStart)

	def getTimer(self):
		return self.timerCount

	def getPause(self):
		return self.pauseCount

	def getActive(self):
		return self.getTimer() - self.getPause()

class Worker:
	def __init__(self, prefix, agentNumber, delay, iterations=1):
		self.agents = []
		self.prefix = prefix
		self.agentNumber = agentNumber
		self.delay = delay
		self.iterations = iterations
		self.scheduleTime = time.time()
		self.process = Process(target=self.main)

	def main(self):
		print("Worker-{} is started with {} agents".format(self.prefix, self.agentNumber))
		for agent in range(0, self.agentNumber):
			self.agents.append(Agent("{}-{:05d}".format(self.prefix, agent), self.iterations))
		for agent in self.agents:
			agent.setScheduleTime(self.delay)
			agent.start()

		while True:
			doneAgents = 0
			for agent in self.agents:
				if not agent.process.isAlive():
					doneAgents += 1
			if doneAgents == self.agentNumber:
				break
			else:
				sleep(1)

	def kill(self):
		for agent in self.agents:
			agent.kill()

	def start(self):
		self.process.start()

class Agent:
	def __init__(self, name, periods=1):
		self.measurement = "agent_status"
		self.name = name
		self.periods = periods
		self.scheduleTime = time.time()
		self.killFlag = False
		self.process = Thread(target=self.main)
		self.session = requests.Session()
		self.process.setDaemon(True)

	def generateCpuMeasurement(self, numbers, timeString):
		cpu_info = []
		cpu_field = ['usr', 'nice', 'sys', 'iowait', 'irq', 'soft', 'steal', 'guest', 'gnice', 'idle']

		for i in range(0, numbers):
			for key in cpu_field:
				cpu_info.append("{}_cpu,agent={},number={},tag={} value={} {}".format(self.measurement, self.name, i, key, random.random() * 100, timeString))
		return cpu_info

	def generateMemoryMeasurement(self, timeString):
		mem_info = []
		mem_field = ['memory_total', 'memory_used', 'memory_free', 'swap_total', 'swap_used', 'swap_free', 'paged_in', 'paged_out', 'swaped_in', 'swaped_out']

		for key in mem_field:
			mem_info.append("{}_memory,agent={},tag={} value={} {}".format(self.measurement, self.name, key, random.random() * 16384, timeString))
		return mem_info

	def generateDmaMeasurement(self, timeString):
		dma_info = []
		dma_field = ['8', '16', '32', '64', '96', '128', '192', '256', '512', '1024', '2048', '4096', '8192']

		for key in dma_field:
			dma_info.append("{}_dma,agent={},size={} value={} {}".format(self.measurement, self.name, key, int(random.random() * 16384), timeString))

		return dma_info

	def generateKernalMeasurement(self, timeString):
		kernal_info = []
		kernal_field = ['8', '16', '32', '64', '96', '128', '192', '256', '512', '1024', '2048', '4096', '8192']

		for key in kernal_field:
			kernal_info.append("{}_kernal,agent={},size={} value={} {}".format(self.measurement, self.name, key, int(random.random() * 16384), timeString))

		return kernal_info

	def generateDiskMeasurement(self, numbers, timeString):
		disk_info = []
		disk_field = ['rps', 'wps', 'rkbps', 'wkbps', 'rrqmps', 'wrqmps', 'rrqm', 'wrqm', 'r_await', 'w_await', 'aqu_sz', 'rareq_sz', 'wareq_sz', 'svctm', 'util']

		for i in range(0, numbers):
			for key in disk_field:
				disk_info.append("{}_disk,agent={},id={},tag={} value={} {}".format(self.measurement, self.name, "sd{}".format(chr(ord('a') + i)), key, random.random() * 100, timeString))
		return disk_info


	def generate(self, ts):
		content = []

		content.extend(self.generateCpuMeasurement(16, ts))
		content.extend(self.generateMemoryMeasurement(ts))
		content.extend(self.generateDmaMeasurement(ts))
		content.extend(self.generateKernalMeasurement(ts))
		content.extend(self.generateDiskMeasurement(4, ts))
		
		return content

	def mainLoop(self):
		Logger.info("Agent - {}: start in {}".format(self.name, time.time()))
		total_count = 0
		partial_count = 0
		interval = 1.0
		ts = int(time.time())

		benchTimer = Timer()
		tickTimer = Timer()
		tickCount = 64
	
		benchTimer.start()	
		tickTimer.start()
		for period in range(0, self.periods):
			if self.killFlag:
				break
			currTs = time.time()
			content = self.generate(generateTimeStamp(ts))
			retry = 5
			#print("{} - content: {}[{}]".format(period, content, len(content)))
			while retry > 0:
				if self.killFlag:
					retry = 0
				req = self.session.post("http://10.11.70.106:8888/write?db=centralize", data="\n".join(content))
				if req.status_code == 204:
					break
				else:
					retry = retry - 1
					Logger.error("Agent - {}({}): post failed [{}: {}], retry times: {}".format(self.name, period, req.status_code, req.text, retry))
			if retry == 0:
				Logger.error("Agent - {}({}): Still failed.".format(self.name, period, req.status_code, req.text, retry))
			
			partial_count = partial_count + len(content)
			total_count = total_count + len(content)
			
			if ((period + 1) % tickCount == 0):
				tickTimer.end()
				Logger.info("Agent - {}: count {} - [{} records in time [{} / {}]]".format(self.name, period + 1, partial_count, tickTimer.getActive(), tickTimer.getTimer()))
				tickTimer.start()
				partial_count = 0
			ts = ts + 5
			benchTimer.pause()
			tickTimer.pause()
			leftTs = 1 - (time.time() - currTs)
			if leftTs > 0:
				sleep(leftTs)
			tickTimer.resume()
			benchTimer.resume()
		benchTimer.end()

		Logger.info("Agent - {}: finish, pushed {} records in time [{} / {}]".format(self.name, total_count, benchTimer.getActive(), benchTimer.getTimer()))

	def main(self):
		schedule = sched.scheduler(time.time, time.sleep)
		delayTime = self.scheduleTime - time.time()
		if delayTime < 0:
			delayTime = 0
		schedule.enter(delayTime, 1, self.mainLoop, ())
		schedule.run()

	def kill(self):
		self.killFlag = True

	def join(self):
		self.process.join()

	def setScheduleTime(self, ts):
		self.scheduleTime = ts

	def start(self):
		self.process.start()

globalWorkers = []

def signal_handler(signal, frame):
	for worker in globalWorkers:
		worker.kill()
		
def main(argv):
	workers = 4
	agents = 128
	periods = 256

	total_agents = workers * agents
	delay = time.time() + (total_agents) / 128
	Logger.info("There are {} agents, all task will be started after {}s".format(total_agents, (total_agents) / 128))

	signal.signal(signal.SIGINT, signal_handler)

	for worker in range(0, workers):
		globalWorkers.append(Worker("Agent{}".format(worker), agents, delay, periods))
	
	for worker in globalWorkers:
		worker.start()

if __name__ == "__main__":
	main(sys.argv[1:])
